import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.openBrowser('')

WebUI.navigateToUrl('http://the-internet.herokuapp.com/upload')

WebUI.waitForPageLoad(10)

def uploadList = findTestData('Upload List Excel')

def rowCount = uploadList.getRowNumbers()

for (def i : (1..rowCount)) {
    def fileName = uploadList.getValue(i, 2)

    def filePath = fileName

    WebUI.uploadFile(findTestObject('Page_The Internet/input_File Uploader_file'), filePath)

    WebUI.click(findTestObject('Object Repository/Page_The Internet/input_File Uploader_file-submit'))

    WebUI.verifyElementText(findTestObject('Object Repository/Page_The Internet/h3_File Uploaded'), 'File Uploaded!')

    WebUI.verifyElementText(findTestObject('Object Repository/Page_The Internet/div_' + fileName), fileName)

    WebUI.navigateToUrl('http://the-internet.herokuapp.com/upload')
}

